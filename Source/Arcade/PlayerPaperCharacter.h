// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include <Camera/CameraComponent.h>
#include "PlayerPaperCharacter.generated.h"

/**
 * 
 */
UCLASS()
class ARCADE_API APlayerPaperCharacter : public APaperCharacter
{
    GENERATED_BODY()

public:

    APlayerPaperCharacter();

    virtual void Tick(float DeltaSeconds) override;

    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    UFUNCTION()
    void UpOrDown(float value);

    UFUNCTION()
    void LeftOrRight(float value);

    UFUNCTION()
    void Fire();

    UPROPERTY(VisibleAnywhere)
    UCameraComponent* PaperCharacterCamera;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
    FVector MuzzleOffset;

    UPROPERTY(EditDefaultsOnly, Category = Projectile)
    TSubclassOf<class AShotProjectile> ProjectileClass;

private:
    class UPaperFlipbook* TextureUp;
    class UPaperFlipbook* TextureDown;
    class UPaperFlipbook* TextureLeft;
    class UPaperFlipbook* TextureRight;

    class UPaperFlipbook* TextureUpIdle;
    class UPaperFlipbook* TextureDownIdle;
    class UPaperFlipbook* TextureLeftIdle;
    class UPaperFlipbook* TextureRightIdle;

    void SetMyFlipbook();
    void SetUpPaperFlipbookTexture();

    enum class MoveDirections { DOWN, UP, LEFT, RIGHT };

    MoveDirections LastMoveDirection{ MoveDirections::UP };

};
