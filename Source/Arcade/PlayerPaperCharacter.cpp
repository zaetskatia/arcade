// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerPaperCharacter.h"
#include <GameFramework/Actor.h>
#include <../Plugins/2D/Paper2D/Source/Paper2D/Classes/PaperFlipbookComponent.h>
#include <../Plugins/2D/Paper2D/Source/Paper2D/Classes/PaperCharacter.h>
#include "Components/CapsuleComponent.h"
#include <GameFramework/Character.h>
#include "GameFramework/CharacterMovementComponent.h"
#include "ConstructorHelpers.h"
#include <GameFramework/Pawn.h>
#include <../Plugins/2D/Paper2D/Source/Paper2D/Classes/PaperFlipbook.h>
#include "ShotProjectile.h"

APlayerPaperCharacter::APlayerPaperCharacter()
{
    PrimaryActorTick.bCanEverTick = true;

    GetCapsuleComponent()->InitCapsuleSize(5.0f, 5.0f);
    
    PaperCharacterCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    check(PaperCharacterCamera != nullptr);

    PaperCharacterCamera->SetupAttachment(CastChecked<USceneComponent, UPaperFlipbookComponent>(GetSprite()));
    PaperCharacterCamera->SetRelativeLocation(FVector(0, 60, 0));
    PaperCharacterCamera->SetRelativeRotation(FRotator(0, -90, 0));

    GetCharacterMovement()->MaxFlySpeed = 100.0f;
    GetCharacterMovement()->BrakingDecelerationFlying = 200.0f;
    GetCharacterMovement()->DefaultLandMovementMode = MOVE_Flying;

    SetUpPaperFlipbookTexture();
}

void APlayerPaperCharacter::Tick(float DeltaSeconds)
{
    SetMyFlipbook();
}

void APlayerPaperCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    PlayerInputComponent->BindAxis("UpOrDown", this, &APlayerPaperCharacter::UpOrDown);
    PlayerInputComponent->BindAxis("LeftOrRight", this, &APlayerPaperCharacter::LeftOrRight);
    PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APlayerPaperCharacter::Fire);
}

void APlayerPaperCharacter::UpOrDown(float value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Z);
    AddMovementInput(Direction, value);
}

void APlayerPaperCharacter::LeftOrRight(float value)
{
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
    AddMovementInput(Direction, value);
}

void APlayerPaperCharacter::Fire()
{
    if (ProjectileClass)
    {
        FVector CameraLocation;
        FRotator CameraRotation;
        GetActorEyesViewPoint(CameraLocation, CameraRotation);

        MuzzleOffset.Set(100.0f, 0.0f, 0.0f);

        FVector MuzzleLocation = CameraLocation + FTransform(CameraRotation).TransformVector(MuzzleOffset);

        FRotator MuzzleRotation = CameraRotation;
        MuzzleRotation.Pitch += 10.0f;

        UWorld* World = GetWorld();
        if (World)
        {
            FActorSpawnParameters SpawnParameters;
            SpawnParameters.Owner = this;
            SpawnParameters.Instigator = GetInstigator();

            AShotProjectile* Projectile = World->SpawnActor<AShotProjectile>(ProjectileClass, MuzzleLocation, MuzzleRotation, SpawnParameters);

            if (Projectile)
            {
                FVector LaunchDirection = MuzzleRotation.Vector();
                Projectile->FireInDirection(LaunchDirection);
            }
        }
    }
}

void APlayerPaperCharacter::SetMyFlipbook()
{
    if (GetInputAxisValue("UpOrDown") > 0)
    {
        LastMoveDirection = MoveDirections::UP;
    }
    else if (GetInputAxisValue("UpOrDown") < 0)
    {
        LastMoveDirection = MoveDirections::DOWN;
    }
    else if (GetInputAxisValue("LeftOrRight") > 0)
    {
        LastMoveDirection = MoveDirections::RIGHT;
    }
    else if (GetInputAxisValue("LeftOrRight") < 0)
    {
        LastMoveDirection = MoveDirections::LEFT;
    }

    switch (LastMoveDirection)
    {
    case APlayerPaperCharacter::MoveDirections::DOWN:
        if (GetVelocity().Size() > 0)
            GetSprite()->SetFlipbook(TextureDown);
        else
            GetSprite()->SetFlipbook(TextureDownIdle);
        break;
    case APlayerPaperCharacter::MoveDirections::UP:
        if (GetVelocity().Size() > 0)
            GetSprite()->SetFlipbook(TextureUp);
        else
            GetSprite()->SetFlipbook(TextureUpIdle);
        break;
    case APlayerPaperCharacter::MoveDirections::LEFT:
        if (GetVelocity().Size() > 0)
            GetSprite()->SetFlipbook(TextureLeft);
        else
            GetSprite()->SetFlipbook(TextureLeftIdle);
        break;
    case APlayerPaperCharacter::MoveDirections::RIGHT:
        if (GetVelocity().Size() > 0)
            GetSprite()->SetFlipbook(TextureRight);
        else
            GetSprite()->SetFlipbook(TextureRightIdle);
        break;
    }
}

void APlayerPaperCharacter::SetUpPaperFlipbookTexture()
{
    TextureUp = ConstructorHelpers::FObjectFinder<UPaperFlipbook>(TEXT("/Game/Characters/MoveUp")).Object;
    TextureDown = ConstructorHelpers::FObjectFinder<UPaperFlipbook>(TEXT("/Game/Characters/MoveDown")).Object;
    TextureLeft = ConstructorHelpers::FObjectFinder<UPaperFlipbook>(TEXT("/Game/Characters/MoveLeft")).Object;
    TextureRight = ConstructorHelpers::FObjectFinder<UPaperFlipbook>(TEXT("/Game/Characters/MoveRight")).Object;

    ConstructorHelpers::FObjectFinder<UPaperFlipbook>PaperFlipbookIdleUp
    (TEXT("/Game/Characters/IdleUp"));
    if (PaperFlipbookIdleUp.Succeeded())
    {
        TextureUpIdle = PaperFlipbookIdleUp.Object;
    }

    ConstructorHelpers::FObjectFinder<UPaperFlipbook>PaperFlipbookIldeDown
    (TEXT("/Game/Characters/EyeUpDown"));
    if (PaperFlipbookIldeDown.Succeeded())
    {
        TextureDownIdle = PaperFlipbookIldeDown.Object;
    }

    ConstructorHelpers::FObjectFinder<UPaperFlipbook>PaperFlipbookIdleLeft
    (TEXT("/Game/Characters/IdleLeft"));
    if (PaperFlipbookIdleLeft.Succeeded())
    {
        TextureLeftIdle = PaperFlipbookIdleLeft.Object;
    }

    ConstructorHelpers::FObjectFinder<UPaperFlipbook>PaperFlipbookIdleRight
    (TEXT("/Game/Characters/IdleRight"));
    if (PaperFlipbookIdleRight.Succeeded())
    {
        TextureRightIdle = PaperFlipbookIdleRight.Object;
    }
}
