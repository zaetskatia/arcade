// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Arcade/PlayerPaperCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerPaperCharacter() {}
// Cross Module References
	ARCADE_API UClass* Z_Construct_UClass_APlayerPaperCharacter_NoRegister();
	ARCADE_API UClass* Z_Construct_UClass_APlayerPaperCharacter();
	PAPER2D_API UClass* Z_Construct_UClass_APaperCharacter();
	UPackage* Z_Construct_UPackage__Script_Arcade();
	ARCADE_API UFunction* Z_Construct_UFunction_APlayerPaperCharacter_Fire();
	ARCADE_API UFunction* Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight();
	ARCADE_API UFunction* Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ARCADE_API UClass* Z_Construct_UClass_AShotProjectile_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	void APlayerPaperCharacter::StaticRegisterNativesAPlayerPaperCharacter()
	{
		UClass* Class = APlayerPaperCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Fire", &APlayerPaperCharacter::execFire },
			{ "LeftOrRight", &APlayerPaperCharacter::execLeftOrRight },
			{ "UpOrDown", &APlayerPaperCharacter::execUpOrDown },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APlayerPaperCharacter_Fire_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerPaperCharacter_Fire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayerPaperCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerPaperCharacter_Fire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerPaperCharacter, "Fire", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerPaperCharacter_Fire_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APlayerPaperCharacter_Fire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerPaperCharacter_Fire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayerPaperCharacter_Fire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics
	{
		struct PlayerPaperCharacter_eventLeftOrRight_Parms
		{
			float value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::NewProp_value = { UE4CodeGen_Private::EPropertyClass::Float, "value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PlayerPaperCharacter_eventLeftOrRight_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayerPaperCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerPaperCharacter, "LeftOrRight", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, sizeof(PlayerPaperCharacter_eventLeftOrRight_Parms), Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics
	{
		struct PlayerPaperCharacter_eventUpOrDown_Parms
		{
			float value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::NewProp_value = { UE4CodeGen_Private::EPropertyClass::Float, "value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PlayerPaperCharacter_eventUpOrDown_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "PlayerPaperCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APlayerPaperCharacter, "UpOrDown", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, sizeof(PlayerPaperCharacter_eventUpOrDown_Parms), Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APlayerPaperCharacter_NoRegister()
	{
		return APlayerPaperCharacter::StaticClass();
	}
	struct Z_Construct_UClass_APlayerPaperCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectileClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ProjectileClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MuzzleOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MuzzleOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaperCharacterCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaperCharacterCamera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APlayerPaperCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APaperCharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_Arcade,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APlayerPaperCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APlayerPaperCharacter_Fire, "Fire" }, // 2883797304
		{ &Z_Construct_UFunction_APlayerPaperCharacter_LeftOrRight, "LeftOrRight" }, // 2278930253
		{ &Z_Construct_UFunction_APlayerPaperCharacter_UpOrDown, "UpOrDown" }, // 3101456950
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaperCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "PlayerPaperCharacter.h" },
		{ "ModuleRelativePath", "PlayerPaperCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_ProjectileClass_MetaData[] = {
		{ "Category", "Projectile" },
		{ "ModuleRelativePath", "PlayerPaperCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_ProjectileClass = { UE4CodeGen_Private::EPropertyClass::Class, "ProjectileClass", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0014000000010001, 1, nullptr, STRUCT_OFFSET(APlayerPaperCharacter, ProjectileClass), Z_Construct_UClass_AShotProjectile_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_ProjectileClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_ProjectileClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_MuzzleOffset_MetaData[] = {
		{ "Category", "Gameplay" },
		{ "ModuleRelativePath", "PlayerPaperCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_MuzzleOffset = { UE4CodeGen_Private::EPropertyClass::Struct, "MuzzleOffset", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(APlayerPaperCharacter, MuzzleOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_MuzzleOffset_MetaData, ARRAY_COUNT(Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_MuzzleOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_PaperCharacterCamera_MetaData[] = {
		{ "Category", "PlayerPaperCharacter" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "PlayerPaperCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_PaperCharacterCamera = { UE4CodeGen_Private::EPropertyClass::Object, "PaperCharacterCamera", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x00100000000a0009, 1, nullptr, STRUCT_OFFSET(APlayerPaperCharacter, PaperCharacterCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_PaperCharacterCamera_MetaData, ARRAY_COUNT(Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_PaperCharacterCamera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APlayerPaperCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_ProjectileClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_MuzzleOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APlayerPaperCharacter_Statics::NewProp_PaperCharacterCamera,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APlayerPaperCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APlayerPaperCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APlayerPaperCharacter_Statics::ClassParams = {
		&APlayerPaperCharacter::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_APlayerPaperCharacter_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_APlayerPaperCharacter_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_APlayerPaperCharacter_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_APlayerPaperCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APlayerPaperCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APlayerPaperCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlayerPaperCharacter, 2388944250);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayerPaperCharacter(Z_Construct_UClass_APlayerPaperCharacter, &APlayerPaperCharacter::StaticClass, TEXT("/Script/Arcade"), TEXT("APlayerPaperCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerPaperCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
