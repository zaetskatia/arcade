// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCADE_SpawnerActor_generated_h
#error "SpawnerActor.generated.h already included, missing '#pragma once' in SpawnerActor.h"
#endif
#define ARCADE_SpawnerActor_generated_h

#define Arcade_Source_Arcade_SpawnerActor_h_12_RPC_WRAPPERS
#define Arcade_Source_Arcade_SpawnerActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcade_Source_Arcade_SpawnerActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnerActor(); \
	friend struct Z_Construct_UClass_ASpawnerActor_Statics; \
public: \
	DECLARE_CLASS(ASpawnerActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(ASpawnerActor)


#define Arcade_Source_Arcade_SpawnerActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASpawnerActor(); \
	friend struct Z_Construct_UClass_ASpawnerActor_Statics; \
public: \
	DECLARE_CLASS(ASpawnerActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(ASpawnerActor)


#define Arcade_Source_Arcade_SpawnerActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnerActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnerActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnerActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnerActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnerActor(ASpawnerActor&&); \
	NO_API ASpawnerActor(const ASpawnerActor&); \
public:


#define Arcade_Source_Arcade_SpawnerActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnerActor(ASpawnerActor&&); \
	NO_API ASpawnerActor(const ASpawnerActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnerActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnerActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnerActor)


#define Arcade_Source_Arcade_SpawnerActor_h_12_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_SpawnerActor_h_9_PROLOG
#define Arcade_Source_Arcade_SpawnerActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_SpawnerActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_SpawnerActor_h_12_RPC_WRAPPERS \
	Arcade_Source_Arcade_SpawnerActor_h_12_INCLASS \
	Arcade_Source_Arcade_SpawnerActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_SpawnerActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_SpawnerActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_SpawnerActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_SpawnerActor_h_12_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_SpawnerActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_SpawnerActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
