// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCADE_ShotProjectile_generated_h
#error "ShotProjectile.generated.h already included, missing '#pragma once' in ShotProjectile.h"
#endif
#define ARCADE_ShotProjectile_generated_h

#define Arcade_Source_Arcade_ShotProjectile_h_14_RPC_WRAPPERS
#define Arcade_Source_Arcade_ShotProjectile_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcade_Source_Arcade_ShotProjectile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShotProjectile(); \
	friend struct Z_Construct_UClass_AShotProjectile_Statics; \
public: \
	DECLARE_CLASS(AShotProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(AShotProjectile)


#define Arcade_Source_Arcade_ShotProjectile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAShotProjectile(); \
	friend struct Z_Construct_UClass_AShotProjectile_Statics; \
public: \
	DECLARE_CLASS(AShotProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(AShotProjectile)


#define Arcade_Source_Arcade_ShotProjectile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShotProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShotProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShotProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShotProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShotProjectile(AShotProjectile&&); \
	NO_API AShotProjectile(const AShotProjectile&); \
public:


#define Arcade_Source_Arcade_ShotProjectile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShotProjectile(AShotProjectile&&); \
	NO_API AShotProjectile(const AShotProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShotProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShotProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AShotProjectile)


#define Arcade_Source_Arcade_ShotProjectile_h_14_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_ShotProjectile_h_11_PROLOG
#define Arcade_Source_Arcade_ShotProjectile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_ShotProjectile_h_14_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_ShotProjectile_h_14_RPC_WRAPPERS \
	Arcade_Source_Arcade_ShotProjectile_h_14_INCLASS \
	Arcade_Source_Arcade_ShotProjectile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_ShotProjectile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_ShotProjectile_h_14_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_ShotProjectile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_ShotProjectile_h_14_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_ShotProjectile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_ShotProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
