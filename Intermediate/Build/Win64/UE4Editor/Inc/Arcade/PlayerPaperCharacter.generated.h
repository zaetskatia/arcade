// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCADE_PlayerPaperCharacter_generated_h
#error "PlayerPaperCharacter.generated.h already included, missing '#pragma once' in PlayerPaperCharacter.h"
#endif
#define ARCADE_PlayerPaperCharacter_generated_h

#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execLeftOrRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->LeftOrRight(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpOrDown) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpOrDown(Z_Param_value); \
		P_NATIVE_END; \
	}


#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->Fire(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execLeftOrRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->LeftOrRight(Z_Param_value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpOrDown) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpOrDown(Z_Param_value); \
		P_NATIVE_END; \
	}


#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPaperCharacter(); \
	friend struct Z_Construct_UClass_APlayerPaperCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayerPaperCharacter, APaperCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(APlayerPaperCharacter)


#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPaperCharacter(); \
	friend struct Z_Construct_UClass_APlayerPaperCharacter_Statics; \
public: \
	DECLARE_CLASS(APlayerPaperCharacter, APaperCharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(APlayerPaperCharacter)


#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPaperCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPaperCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPaperCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPaperCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPaperCharacter(APlayerPaperCharacter&&); \
	NO_API APlayerPaperCharacter(const APlayerPaperCharacter&); \
public:


#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPaperCharacter(APlayerPaperCharacter&&); \
	NO_API APlayerPaperCharacter(const APlayerPaperCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPaperCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPaperCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPaperCharacter)


#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_PlayerPaperCharacter_h_13_PROLOG
#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_PlayerPaperCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_PlayerPaperCharacter_h_16_RPC_WRAPPERS \
	Arcade_Source_Arcade_PlayerPaperCharacter_h_16_INCLASS \
	Arcade_Source_Arcade_PlayerPaperCharacter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_PlayerPaperCharacter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_PlayerPaperCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_PlayerPaperCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_PlayerPaperCharacter_h_16_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_PlayerPaperCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_PlayerPaperCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
