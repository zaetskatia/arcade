// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARCADE_ArcadeGameModeBase_generated_h
#error "ArcadeGameModeBase.generated.h already included, missing '#pragma once' in ArcadeGameModeBase.h"
#endif
#define ARCADE_ArcadeGameModeBase_generated_h

#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_RPC_WRAPPERS
#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArcadeGameModeBase(); \
	friend struct Z_Construct_UClass_AArcadeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AArcadeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(AArcadeGameModeBase)


#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAArcadeGameModeBase(); \
	friend struct Z_Construct_UClass_AArcadeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AArcadeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Arcade"), NO_API) \
	DECLARE_SERIALIZER(AArcadeGameModeBase)


#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArcadeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArcadeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArcadeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcadeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArcadeGameModeBase(AArcadeGameModeBase&&); \
	NO_API AArcadeGameModeBase(const AArcadeGameModeBase&); \
public:


#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArcadeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArcadeGameModeBase(AArcadeGameModeBase&&); \
	NO_API AArcadeGameModeBase(const AArcadeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArcadeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcadeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArcadeGameModeBase)


#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Arcade_Source_Arcade_ArcadeGameModeBase_h_12_PROLOG
#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_ArcadeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_ArcadeGameModeBase_h_15_RPC_WRAPPERS \
	Arcade_Source_Arcade_ArcadeGameModeBase_h_15_INCLASS \
	Arcade_Source_Arcade_ArcadeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Arcade_Source_Arcade_ArcadeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Arcade_Source_Arcade_ArcadeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Arcade_Source_Arcade_ArcadeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Arcade_Source_Arcade_ArcadeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Arcade_Source_Arcade_ArcadeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Arcade_Source_Arcade_ArcadeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
